insert into customers (id, name, email)
values (1, 'Белый медвель', 'bear@mail.ru'),
       (2, 'Зооторг', 'zootorg@yandex.ru'),
       (3, 'ОАО Кошкин дом', 'cat_house@gmail.com'),
       (4, 'Любимец', 'love_cat@mail.ru'),
       (5, 'Ветклиника', 'zoovet@gmail.com');

insert into products(id, name, price)
values (1, 'Animonda', 103),
       (2, 'Applaws', 120),
       (3, 'Barking (Meowing) Heads', 140),
       (4, 'Berkley', 90),
       (5, 'Grandorf', 76),
       (6, 'Monge', 99);

insert into orders(id, customer_id, product_id, quantity)
values (1, 1, 1, 100),
       (2, 2, 2, 100),
       (3, 3, 3, 100),
       (4, 4, 4, 100),
       (5, 5, 5, 100);