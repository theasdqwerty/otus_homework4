create table customers
(
    id serial primary key,
    name character varying(255)  not null,
    email character varying(255)  not null,

    constraint customers_email_unique unique (email)
);

create table orders
(
    id serial primary key,
    customer_id bigint not null,
    product_id bigint not null,
    quantity bigint not null,

    constraint orders_fk_customers_id foreign key (customer_id) references customers(id) on delete cascade
);

create table products
(
    id serial primary key,
    name character varying(255) not null,
    price bigint not null
);