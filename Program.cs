﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Npgsql;

namespace dotnetcore
{
    class Program
    {
        /// <summary>
        /// Строка подключения к базе данных.
        /// </summary>
        private static string connectionString =
            "Host=localhost;Port=6999;Username=postgres;Password=postgrespw;Database=postgres";

        private static void Main()
        {
            using (var connection = new NpgsqlConnection(connectionString))
            {
                connection.Open();

                var createTablesSql = new FileInfo(@"CreateTables.sql");
                using (var createTablesCommand = new NpgsqlCommand(createTablesSql.OpenText().ReadToEnd(), connection))
                    createTablesCommand.ExecuteNonQuery();
                
                var insertDataSql = new FileInfo(@"InsertData.sql");
                using (var insertDataCommand = new NpgsqlCommand(insertDataSql.OpenText().ReadToEnd(), connection))
                    insertDataCommand.ExecuteNonQuery();

                ShowData(connection);

                AddCustomer(connection);
            
                connection.Close();
            }
        }
        
        /// <summary>
        /// Добавление записи в таблицу.
        /// </summary>
        /// <param name="connection">Соединение с БД.</param>
        private static void AddCustomer(NpgsqlConnection connection)
        {
            Console.WriteLine("Добавление записи.");
            Console.WriteLine("Введите имя:");
            var name = Console.ReadLine();
            Console.WriteLine("Введите email:");
            var email = Console.ReadLine();

            using (var command = new NpgsqlCommand(@"insert into customers(name, email) values (@name, @email)",
                       connection))
            {
                command.Parameters.Add(new NpgsqlParameter(@"name", name));
                command.Parameters.Add(new NpgsqlParameter(@"email", email));

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Показать данные.
        /// </summary>
        /// <param name="connection">Соединение с БД.</param>
        private static void ShowData(NpgsqlConnection connection)
        {
            Console.WriteLine("Показать содержимое таблицы.");
            var tables = new Dictionary<string, int>()
            {
                { "customers", 1 },
                { "products", 2 },
                { "order", 3 },
                { "Выход", 0 },
            };

            while (true)
            {
                Console.WriteLine("Выбор таблицы:");
                foreach (var tableName in tables.Keys)
                    Console.WriteLine($"{tables[tableName]}:\t{tableName}");

                int tableKey;
                if (!int.TryParse(Console.ReadLine(), out tableKey))
                    continue;
                
                if (tableKey == 0)
                    break;

                if (!tables.Values.Contains(tableKey))
                    continue;

                switch (tableKey)
                {
                    case 1:
                    {
                        using (var command = new NpgsqlCommand(@"select name,email from customers", connection))
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                    Console.WriteLine($"Name: {reader.GetString(0)}, email: {reader.GetString(1)}");
                            }
                        }
                        break;
                    }
                    
                    case 2:
                    {
                        using (var command = new NpgsqlCommand(@"select name,price from products", connection))
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                    Console.WriteLine($"Name: {reader.GetString(0)}, Price: {reader.GetInt32(1)}");
                            }
                        }
                        break;
                    }

                    case 3:
                    {
                        using (var command = new NpgsqlCommand(@"select customer_id,product_id,quantity from orders", connection))
                        {
                            using (var reader = command.ExecuteReader())
                            {
                                while (reader.Read())
                                    Console.WriteLine($"Customer_id: {reader.GetInt32(0)}, product_id: {reader.GetInt32(1)}, quantity: {reader.GetInt32(2)}");
                            }
                        }
                        break;
                    }
                }
            }
        }
    }
}